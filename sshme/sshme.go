package sshme

import (
	"fmt"
	"io"
	//"io/ioutil"
	"os"
	"strings"
	"regexp"
	"io/ioutil"
	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"
)

type SSHCommand struct {
	Path   string
	Env    []string
	Stdin  io.Reader
	Stdout io.Writer
	Stderr io.Writer
}

type SSHClient struct {
	Config *ssh.ClientConfig
	Host   string
	Port   int
}

func (client *SSHClient) RunCommand(cmd *SSHCommand) error {
	var (
		session *ssh.Session
		err     error
	)

	if session, err = client.newSession(); err != nil {
		return err
	}
	defer session.Close()

	if err = client.prepareCommand(session, cmd); err != nil {
		return err
	}

	err = session.Run(cmd.Path)
	return err
}

func (client *SSHClient) prepareCommand(session *ssh.Session, cmd *SSHCommand) error {
	for _, env := range cmd.Env {
		variable := strings.Split(env, "=")
		if len(variable) != 2 {
			continue
		}

		if err := session.Setenv(variable[0], variable[1]); err != nil {
			return err
		}
	}

	if cmd.Stdin != nil {
		stdin, err := session.StdinPipe()
		if err != nil {
			return fmt.Errorf("Unable to setup stdin for session: %v", err)
		}
		go io.Copy(stdin, cmd.Stdin)
	}

	if cmd.Stdout != nil {
		stdout, err := session.StdoutPipe()
		if err != nil {
			return fmt.Errorf("Unable to setup stdout for session: %v", err)
		}
		go io.Copy(cmd.Stdout, stdout)
	}

	if cmd.Stderr != nil {
		stderr, err := session.StderrPipe()
		if err != nil {
			return fmt.Errorf("Unable to setup stderr for session: %v", err)
		}
		go io.Copy(cmd.Stderr, stderr)
	}

	return nil
}

func (client *SSHClient) newSession() (*ssh.Session, error) {
	connection, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", client.Host, client.Port), client.Config)
	if err != nil {
		return nil, fmt.Errorf("Failed to dial: %s", err)
	}

	session, err := connection.NewSession()
	if err != nil {
		return nil, fmt.Errorf("Failed to create session: %s", err)
	}

	modes := ssh.TerminalModes{
		// ssh.ECHO:          0,     // disable echoing
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	}

	if err := session.RequestPty("xterm", 100, 200, modes); err != nil {
		session.Close()
		return nil, fmt.Errorf("request for pseudo terminal failed: %s", err)
	}

	return session, nil
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}


func Run() {

	viper.SetConfigName("config")
    viper.AddConfigPath("/root/work/src/vishnu")
	err := viper.ReadInConfig()
    if err != nil {
        panic(err)
    }
	viper.WatchConfig()
    Dictionnary := viper.Get("Dictionnary")
    SSHPort := viper.Get("SSHPort")
    dlign, err := ioutil.ReadFile(Dictionnary.(string))
    check(err)
    fmt.Print(string(dlign))
	rp1, err := regexp.Compile(".+:")
	users :=rp1.FindAllString(string(dlign), -1)// type id []string
	fmt.Println(users)
	rp2, err := regexp.Compile(":.+")
	passwords :=rp2.FindAllString(string(dlign), -1)// type id []string
	fmt.Println(passwords)
	// TODO : Must try/except for every usercleaned/passworcleaned my ssh connection
	for j := 0; j <= len(users)-1;j++ {
		usercleaned :=strings.TrimSuffix(users[j],":")
		passwordcleaned :=strings.TrimPrefix(passwords[j],":")
		fmt.Println(usercleaned)
		fmt.Println(passwordcleaned)
	//}
	sshConfig := &ssh.ClientConfig{
		User: usercleaned,
		Auth: []ssh.AuthMethod{
			ssh.Password(passwordcleaned),
		},
	}
//TODO : Must fix this fixed ip
	fmt.Println(SSHPort)
	client := &SSHClient{
		Config: sshConfig,
		Host:   "127.0.0.1",
		Port:   SSHPort.(int),
	}

	cmdgcc := &SSHCommand{
		Path:   "which gcc",
		Env:    []string{"LC_DIR=/"},
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}
	cmdlsb := &SSHCommand{
		Path:   "lsb_release -a",
		Env:    []string{"LC_DIR=/"},
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}

	cmduname := &SSHCommand{
		Path:   "uname -r",
		Env:    []string{"LC_DIR=/"},
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}
	cmdps := &SSHCommand{
		Path:   "ps faux",
		Env:    []string{"LC_DIR=/"},
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}


	fmt.Printf("Running command: %s\n", cmdlsb.Path)
	if err := client.RunCommand(cmdlsb); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot connect to host with login/password : %s/%s\n", usercleaned, passwordcleaned)
	} else {
		fmt.Printf("Successfully logged in with login/password : %s/%s\n\n", usercleaned, passwordcleaned)
		fmt.Printf("Running command: %s\n", cmdgcc.Path)
		if err := client.RunCommand(cmdgcc); err != nil {
			fmt.Fprintf(os.Stderr, "Cannot get gcc: %s\n", err)
		}
		fmt.Printf("Running command: %s\n", cmduname.Path)
		if err := client.RunCommand(cmduname); err != nil {
			fmt.Fprintf(os.Stderr, "Cannot get uname: %s\n", err)
		}
		fmt.Printf("Running command: %s\n", cmdps.Path)
		if err := client.RunCommand(cmdps); err != nil {
			fmt.Fprintf(os.Stderr, "Cannot get ps: %s\n", err)
		}

        }
   }
}

