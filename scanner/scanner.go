// scanner
package scanner

import (
	"fmt"
	"log"
	"bytes"
	"net"
	"time"
	"vishnu/sshme"
)


func ScanNetwork(port string,) {
	var status string
	var buffer bytes.Buffer
	ip, ipnet, err := net.ParseCIDR("172.17.91.0/24")
	if err != nil {
		log.Fatal(err)
	}
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		fmt.Println(ip)
		buffer.WriteString(ip.String())
		buffer.WriteString(":")
		buffer.WriteString(port)
		//fmt.Println(buffer.String())
		conn, err := net.DialTimeout("tcp", buffer.String(), 2 * time.Millisecond)
		if err != nil {
			//log.Println("Connection error:", err)
			status = "Unreachable"
		} else {
			fmt.Println(buffer.String())
			status = "Online"
			sshme.Run()
			defer conn.Close()
		}
		fmt.Println(status)
		buffer.Reset()
	}
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

