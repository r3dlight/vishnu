// main
package main

import (
	"vishnu/scanner"
	//"vishnu/getip"
	"fmt"
	"net"
	"regexp"
	//"io/ioutil"
	//"github.com/spf13/viper"
	//"reflect"
)

func localAddresses() []net.Addr {
    ifaces, err := net.Interfaces()
    if err != nil {
        fmt.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
    }
	y := 0
	ipsSlice := make([]net.Addr, 9)
    for _, i := range ifaces {
        addrs, err := i.Addrs()
        if err != nil {
            fmt.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
        }
        for _, a := range addrs {
            //fmt.Printf("%v %v\n", i.Name, a)
			r, _ := regexp.Compile("127.0.0.1")
			if r.MatchString(a.String()) == false {
				//if a.To4 != nil {
					//TODO : check ipv6
				//fmt.Printf("%v\n", a)
				//}
				ipsSlice[y] = a
				y++
			}
        }
    }
	return ipsSlice
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}


func main() {

	ips := localAddresses() //ips is a slice []net.Addr
	fmt.Println("%v", ips)
	scanner.ScanNetwork("22")
}
